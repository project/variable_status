<?php

/**
 * @file
 * Custom functions for Variable Status.
 */

/**
 * Load the variables and their locked status.
 *
 * @return array
 *   Variables from strongarm and the settings file.
 */
function variable_status_get_variables($reset = FALSE) {
  $variables = &drupal_static(__FUNCTION__);

  if ($reset) {
    cache_clear_all('variable_status');
    unset($variables);
  }

  // No static cache.  Dig deeper.
  if (!isset($variables)) {
    // See if the variable status is stored in cache.
    $cache = cache_get('variable_status');

    if (!isset($cache->data)) {
      // First, get the strongarm variables.
      $strongarm = _variable_status_get_strongarm_vars();

      // Next, get the $conf variables from settings.php.
      $settings = _variable_status_get_settings_vars();

      // Give the settings file vars precedence.
      $variables = $settings + $strongarm;

      // Allow other modules to alter the variables.
      drupal_alter('variable_status', $variables);

      // Store the status in cache.
      cache_set('variable_status', $variables);
    }
    else {
      $variables = $cache->data;
    }
  }

  return $variables;
}

/**
 * Recurse through the elements to find and mark form fields.
 *
 * @param array $element
 *   An element to traverse, such as a $form array.
 * @param array $variables
 *   The system vars to check.
 * @param array $types_to_check
 *   The form element types to validate.
 */
function variable_status_check_locked_form_fields(array &$element, array $variables, array $types_to_check = array('textfield', 'textarea', 'checkbox', 'checkboxes', 'radio', 'radios', 'password')) {
  foreach (element_children($element) as $form_element) {
    // Recurse down the form tree to check all children first.
    variable_status_check_locked_form_fields($element[$form_element], $variables, $types_to_check);

    if (isset($variables[$form_element])) {
      // If the variable is an array, see if it's a tree'd value.
      if (is_array($variables[$form_element])) {
        variable_status_check_locked_form_fields($element[$form_element], $variables[$form_element], $types_to_check);
      }
      elseif (isset($element[$form_element]['#type']) && in_array($element[$form_element]['#type'], $types_to_check)) {
        if ($variables[$form_element] === VARIABLE_STATUS_LOCKED) {
          $element[$form_element]['#disabled'] = TRUE;
          $desc = '<span class="variable-status variable-status--locked">' . t("Value is stored in code and is not editable.") . '</span>';
          _variable_status_add_description($element[$form_element], $desc);
        }
        elseif ($variables[$form_element] === VARIABLE_STATUS_IN_CODE) {
          if (_variable_status_user_override_strongarm()) {
            $element[$form_element]['#disabled'] = TRUE;
            $desc = '<span class="variable-status variable-status--locked">' . t("Value is stored in code and you do not have permission to override it.") . '</span>';
            _variable_status_add_description($element[$form_element], $desc);
          }
          else {
            $desc = '<span class="variable-status variable-status--in-code">' . t("Value is stored in code. Changes may not be permanent.") . '</span>';
            _variable_status_add_description($element[$form_element], $desc);
          }
        }
      }
    }
  }
}

/**
 * Get the variables from Strongarm.
 *
 * @return array
 *   All variables stored in the strongarm settings.
 */
function _variable_status_get_strongarm_vars() {
  $vars = array();

  if (module_exists('strongarm')) {
    foreach (strongarm_vars_load() as $name => $object) {
      if ($object->export_type & EXPORT_IN_CODE) {
        $vars[$name] = VARIABLE_STATUS_IN_CODE;
      }
    }
  }

  return $vars;
}

/**
 * Get the variables from the settings.php file.
 *
 * @return array
 *   All $conf vars.
 */
function _variable_status_get_settings_vars() {
  $conf = array();
  $settings_path = DRUPAL_ROOT . '/' . conf_path() . '/settings.php';

  // Next, get the $conf variables from settings.php.
  if (file_exists($settings_path)) {
    include $settings_path;
  }

  $locked_vars = array_keys($conf);
  $vars = array_fill_keys($locked_vars, VARIABLE_STATUS_LOCKED);

  return $vars;
}

/**
 * Check the settings to see if strongarmed variables should be locked.
 *
 * @return bool
 *   TRUE to lock fields whose values are strongarmed.
 */
function _variable_status_user_override_strongarm() {
  return user_access('variable_status edit strongarmed variables');
}

/**
 * Append a string to an element description.
 *
 * @param array $element
 *   The form element.
 * @param string $string
 *   The string to add to the description.
 */
function _variable_status_add_description(&$element, $string) {
  $element['#description'] = empty($element['#description']) ? '' : $element['#description'] . '<br />';
  $element['#description'] .= $string;
}
